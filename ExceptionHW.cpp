﻿
#include <iostream>
#include <exception>
#include <sstream>

class Fraction
{
public:
    Fraction()
        : m_Numerator(0), m_Denominator(1)
    {}

    Fraction(int numerator, int denominator)
    {
        if (denominator == 0)
        {
            throw std::runtime_error("Denominator can't be 0");
        }

        m_Numerator = numerator;
        m_Denominator = denominator;
    }

    int GetNumerator() const
    {
        return m_Numerator;
    }

    int GetDenominator() const
    {
        return m_Denominator;
    }

private:
    int m_Numerator;
    int m_Denominator;
};

std::ostream& operator<<(std::ostream& out, const Fraction& f)
{
    return out << f.GetNumerator() << '/' << f.GetDenominator();
}

std::istream& operator>>(std::istream& in,Fraction& a)
{
    int n = 0, d = 0;

    if (in >> n && in >> d)
    {
        a = Fraction(n, d);
    }
    else
    {
        throw std::invalid_argument("Numerator or/and Denominator values format not correct! Only integers allowed");
    }
    return in;
}

int main()
{
    while (true)
    {
        try {
            std::cout << "Enter fraction parts in format 'numerator denominator'\n";
            Fraction f;
            std::string str;
            std::getline(std::cin, str);

            std::istringstream is(str);
            is >> f;
            //std::cin >> f;
            std::cout << "Result fraction: " << f << '\n';
        }
        catch (std::runtime_error& er)
        {
            std::cout << er.what() << '\n';
        }
        catch (std::exception& er)
        {
            std::cout << er.what() << '\n';
            break;
        }
    }

    return 0;
}

